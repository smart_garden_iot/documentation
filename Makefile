## Targets defined are:
#
# make			compile all source code files available
# make clean   	remove pdf files

ASCIIDOC_TOOL := asciidoctor-pdf
ASCIIDOC_ARGS := -a pdf-themesdir=. -a pdf-theme=report
ASCIIDOC_ARGS += -a pdf-fontsdir='assets/fonts/FiraCode;GEM_FONTS_DIR'

DOC = $(wildcard *.adoc)
PDF = $(DOC:.adoc=.pdf)

ifneq ($(SKIP_DOCKER), true)
	DOCKER_IMG  := asciidoctor/docker-asciidoctor
	DOCKER_ARGS := --rm -it -v $(PWD):/documents/:rw
	DOCKER_CMD  := docker pull -q ${DOCKER_IMG} && docker run ${DOCKER_ARGS} ${DOCKER_IMG}
endif

all: $(PDF)

.PHONY: all

%.pdf: %.adoc
	${DOCKER_CMD} ${ASCIIDOC_TOOL} ${ASCIIDOC_ARGS} $^ -o $@

clean:
	rm -f ./*.pdf
